#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
# SPDX-License-Identifier: GPL-3.0-or-later

# You can use this version crontabbed on a server to grab monthly toots for several KDE-associated projects


from mastodon import Mastodon
import os.path
import json
import requests
import argparse
from datetime import datetime
from dateutil.relativedelta import relativedelta

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Download too data from your Mastodon account.')
    parser.add_argument ('-a', '--account',     help = 'account to grab toots from')
    parser.add_argument ('-u', '--instance',    help = 'instance URL (only needed for first time access and registration)')
    parser.add_argument ('-s', '--store',       help = 'name of file where you want to store the data')
    parser.add_argument ('-m', '--month',       help = 'download toots from this month (between 01 and 12)', default = str((datetime.today() - relativedelta(months=1)).month))
    parser.add_argument ('-y', '--year',        help = 'download toots from this year (century and year, i.e. 2023)', default = str((datetime.today() - relativedelta(months=1)).year))
    args=parser.parse_args()

# ---------- REGISTER ----------
    # If credentials file doesn't exits, register and create it
    if not(os.path.isfile(args.account)):
        Mastodon.create_app(
            'tootcollection',
            api_base_url = args.instance,
            to_file = args.account
            )

        mastodon = Mastodon (
            client_id = args.account
            )


        res=requests.get(f"{args.instance}/api/v1/accounts/lookup?acct=@{args.account}")

        print(f"{args.instance}/api/v1/accounts/lookup?acct=@{args.account}")

        with open(args.account, 'a') as file:
            file.write (args.account + '\n')
            file.write (res.json()["id"] + '\n')
# -------------------------------

    f_read = open(args.account, "r")
    lines = f_read.readlines()
    f_read.close()

    instance = lines[-4].strip(" \n")
    userid = lines[-1].strip(" \n")

    masty = Mastodon(
        client_id = args.account,
        api_base_url = instance
    )

    timeline = masty.account_statuses(userid, exclude_replies=True, exclude_reblogs=True, limit=40)#, max_id=109274656483565695)
    fields=["id", "created_at", "url", "content", "replies_count", "reblogs_count", "favourites_count", "media_attachments"]
    with open("/home/paul/KDEShare/Promo Team Resources/Data/Social Media/Mastodon/" + os.path.basename(args.account) + "/" + args.year + args.month.zfill(2) + "_" + args.store, 'w') as datafile:

        header=''
        for field in fields:
            if field != "media_attachments":
                header=header + '§' + field + '§,'
            else:
                for attachment in range (1,5):
                    an = str(attachment)
                    header = header + '§attachment'+ an + '_id§,' + '§attachment'+ an + '_url§,'  + '§attachment'+ an + '_description§,'
        datafile.write (header + '\n')

        for status in timeline:
            if status.content !='':
                toot=''
                if (status.created_at.month == int(args.month) and (status.created_at.year == int(args.year))):
                    for field in fields:
                        if field=="media_attachments":
                            for attachment in status[field]:
                                toot = toot + '§' + str(attachment.id) + '§,'  + '§' + attachment.url + '§,' + '§' + str(attachment.description) + '§,'
                        else:
                            toot = toot + '§' + str((status[field])) + '§,'
                    toot = toot[:-1]
                    datafile.write(toot + '\n')
