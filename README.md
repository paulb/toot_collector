<!---
SPDX-FileCopyrightText: 2023 Paul Brown <paul.brown@kde.org>
SPDX-License-Identifier: CC-BY-4.0
-->
# Toot Collector

Toot collector collects stats from your Mastodon account so you can figure out which toots are popular and which are not. Useful to work out when, how and what to toot. 

## Download

Download a zip, tar, gzip or bzip from the links above, or clone the repository with

```
git clone git@invent.kde.org:paulb/toot_collector.git
```

## Installing dependencies

*toot_collector.py* needs the [Mastodon.py](https://pypi.org/project/Mastodon.py/)  module to work. You can install it with

```
pip install Mastodon.py
```

or

```
pip3 install Mastodon.py
```

## Register the app

The first time you run  *toot_collector.py* it will register the app with the Mastodon instance you want to poll.

With this in mind, you will have to provide...

* ... the URL of the instance you use (e.g. `https://floss.social`)
* ... the name of the account you want to download data from

As the app only accesses publicly available toots, you do not have to provide any credentials.

Make the app executable with

```
chmod a+x toot_collector.py
```

and run it with

```
./toot_collector.py -u "https://your.mastodon.instance" -a AccountName -s file_to_store_toots.csv 
```

*toot_collector.py* will save the information it needs in a file called with the same name as the account in the app's directory. This file does not hold sensitive content, but maybe do not share it all the same.

You can use the ```-h``` flag to get help:

```
./toot_collector.py -h
```

This will show:

```
usage: toot_collector.py [-h] [-a ACCOUNT] [-u INSTANCE] [-s STORE] [-m MONTH] [-y YEAR]

Download too data from your Mastodon account.

options:
  -h, --help            show this help message and exit
  -a ACCOUNT, --account ACCOUNT
                        account to grab toots from
  -u INSTANCE, --instance INSTANCE
                        instance URL (only needed for first time access and registration)
  -s STORE, --store STORE
                        name of file where you want to store the data
  -m MONTH, --month MONTH
                        download toots from this month (between 01 and 12)
  -y YEAR, --year YEAR  download toots from this year (century and year, i.e. 2023)
```

By default, *toot_collector.py* will download the toots of the prior month of the current one.

The next time you run *toot_collector.py*, you don't need to specify the instance...
  
```
 ./toot_collector.py -a AccountName -s file_to_store_toots.csv
```

... will work fine.

## Caveats

*toot_collector.py* is only able to download a maximum of 40 toots at a time, and only the most recent toots. This is a Mastodon API limitation. As it is designed to grab only the toots of a recent month (usually the prior month to the current one), it should be okay.

If you need more toots or to go further back, you will need to tweak the code and use things like max_id, min_id and since_id. 

### References

* [Mastodon.py documentation](https://mastodonpy.readthedocs.io/en/stable/index.html)
* [Mastodon Methods](https://docs.joinmastodon.org/methods/)

